import "./App.css";
import BaiTapLayoutComponent from "./BaiTapLayoutComponent/BaiTapLayoutComponent";

function App() {
  return (
    <div className="App ">
      <BaiTapLayoutComponent />
    </div>
  );
}

export default App;
