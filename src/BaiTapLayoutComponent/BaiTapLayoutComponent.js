import React, { Component } from "react";

export default class BaiTapLayoutComponent extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div class="container px-lg-5">
            <a className="navbar-brand" href="#">
              Start Bootstrap
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item active">
                  <a className="nav-link" href="#">
                    Home <span className="sr-only">(current)</span>
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    About
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <body className="container px-lg-5">
          <div className="jumbotron text-center">
            <h1 className="display-4 font-weight-bold">A warm welcome!</h1>
            <p className="lead">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Labore,
              similique molestiae! Quia deleniti error libero hic, ullam commodi
              fugiat, iste qui explicabo similique enim alias maxime non quidem,
              atque deserunt!
            </p>

            <a className="btn btn-primary btn-lg" href="#" role="button">
              Call to action{" "}
            </a>
          </div>

          <div class="row row-cols-4">
            <div className="col">
              <div className="card text-center" style={{ width: "18rem" }}>
                <img
                  src="https://pic-bstarstatic.akamaized.net/ugc/69c40d260078277711d717e532cf8056.jpeg"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>

            <div class="col">
              <div className="card text-center" style={{ width: "18rem" }}>
                <img
                  src="https://pic-bstarstatic.akamaized.net/ugc/69c40d260078277711d717e532cf8056.jpeg"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>
            <div class="col">
              <div className="card text-center" style={{ width: "18rem" }}>
                <img
                  src="https://pic-bstarstatic.akamaized.net/ugc/69c40d260078277711d717e532cf8056.jpeg"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>
            <div class="col">
              <div className="card text-center" style={{ width: "18rem" }}>
                <img
                  src="https://pic-bstarstatic.akamaized.net/ugc/69c40d260078277711d717e532cf8056.jpeg"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>
          </div>
        </body>

        <footer className="bg-dark text-light p-3 mt-2">
          Copyright @ Your Website 2019
        </footer>
      </div>
    );
  }
}
